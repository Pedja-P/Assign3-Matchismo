//
//  RZDSetCardCollectionView.h
//  GraphicalCardGame
//
//  Created by Predrag Pavlovic on 4/7/13.
//  Copyright (c) 2013 Rezedo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RZDSetCardView.h"

@interface RZDSetCardCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet RZDSetCardView *setCardView;

@end
