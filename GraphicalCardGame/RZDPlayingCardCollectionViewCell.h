//
//  RZDPlayingCardCollectionViewCell.h
//  GraphicalCardGame
//
//  Created by Predrag Pavlovic on 3/31/13.
//  Copyright (c) 2013 Rezedo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RZDPlayingCardView.h"

@interface RZDPlayingCardCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet RZDPlayingCardView *playingCardView;

@end
