//
//  RZDSetGameViewController.m
//  GraphicalCardGame
//
//  Created by Predrag Pavlovic on 3/9/13.
//  Copyright (c) 2013 Rezedo. All rights reserved.
//

#import "RZDSetGameViewController.h"
#import "RZDSetCardDeck.h"
#import "RZDSetCard.h"
#import "RZDGameResult.h"
#import "RZDSetCardCollectionViewCell.h"

@interface RZDSetGameViewController ()

@end

@implementation RZDSetGameViewController

#define STARTING_SET_CARD_COUNT 12
- (NSUInteger)startingCardCount
{
    return STARTING_SET_CARD_COUNT;
}

// returns the name of the game.
- (NSString *)gameName {
    return [RZDGameResult gameSetName];
}

// creates deck
- (RZDDeck *)createDeck
{
    return [[RZDSetCardDeck alloc] init];
}

#define SET_GAME_MODE 2 // 3 card matching game
- (NSUInteger)gameMode
{
    return SET_GAME_MODE;
}

- (BOOL)isDeletionOfCardsEnabled
{
    return YES;
}

#define RESULT_STRING_FONT_SIZE 13.0
- (NSAttributedString *)convertResultStringToAttributed:(NSString *)resultString
                                           matchedCards:(NSArray *)matchedCards
{
    NSMutableAttributedString *mat = [[NSMutableAttributedString alloc] initWithString:resultString];
    /*
    for (id matchedCard in matchedCards) {
        if ([matchedCard isKindOfClass:[RZDCard class]]) {
            RZDCard *card = (RZDCard *)matchedCard;
            NSRange range = [[mat mutableString] rangeOfString:card.contents];
            if (range.location == NSNotFound) break;
            [mat replaceCharactersInRange:range
                     withAttributedString:[self attributedStringFromContentOfCard:card]];
        }
    }
     */
    [mat addAttribute:NSFontAttributeName
                value:[UIFont systemFontOfSize:RESULT_STRING_FONT_SIZE]
                range:NSMakeRange(0, [mat length])];
    return mat;
}

#define SOLID_SET_CARD_ALPHA 1.0
#define STRIPED_SET_CARD_ALPHA 0.2
#define OPEN_SET_CARD_ALPHA 0.0
#define SET_CARD_STROKE_WIDTH -3.0
// creates attributed string of card content
- (NSAttributedString *)attributedStringFromContentOfCard:(RZDCard *)card
{
    NSString *contentAsString = @"";
    NSArray *symbols = @[[NSNull null], @"▲", @"●", @"■"];
    NSArray *colors = @[[NSNull null], [UIColor redColor], [UIColor greenColor],
                        [UIColor purpleColor]];
    NSMutableDictionary *attributes = [[NSMutableDictionary alloc] init];
    
    if (card && [card isKindOfClass:[RZDSetCard class]]) {
        RZDSetCard *setCard = (RZDSetCard *)card;
        CGFloat alpha = OPEN_SET_CARD_ALPHA;
        if (setCard.shading == SetCardShadingOne) {
            alpha = SOLID_SET_CARD_ALPHA;
        } else if (setCard.shading == SetCardShadingTwo) {
            alpha = STRIPED_SET_CARD_ALPHA;
        }
        UIColor *color = [colors[setCard.color] colorWithAlphaComponent:alpha];
        [attributes setObject:color forKey:NSForegroundColorAttributeName];
        [attributes setObject:@(SET_CARD_STROKE_WIDTH) forKey:NSStrokeWidthAttributeName];
        [attributes setObject:colors[setCard.color] forKey:NSStrokeColorAttributeName];
                                     
        contentAsString = [contentAsString stringByPaddingToLength:setCard.rank
                                                       withString:symbols[setCard.symbol]
                                                  startingAtIndex:0];
    }
    return [[NSAttributedString alloc] initWithString:contentAsString attributes:attributes];
}

#define UNPLAYABLE_CARD_ALPHA 0
#define PLAYABLE_CARD_ALPHA 1
- (void)updateCell:(UICollectionViewCell *)cell usingCard:(RZDCard *)card animated:(BOOL)animated
{
    if ([cell isKindOfClass:[RZDSetCardCollectionViewCell class]]) {
        RZDSetCardView *setCardView = ((RZDSetCardCollectionViewCell *)cell).setCardView;
        if ([card isKindOfClass:[RZDSetCard class]]) {
            RZDSetCard *setCard = (RZDSetCard *)card;
            setCardView.rank = setCard.rank;
            setCardView.shading = setCard.shading;
            setCardView.symbol = setCard.symbol;
            setCardView.color = setCard.color;
            setCardView.faceUp = setCard.isFaceUp;
        }
    }
}

// adds card view to flip result view
- (void)addCard:(RZDCard *)card resultView:(UIView *)view inRect:(CGRect)rect
{
    if (card) {
        if ([card isKindOfClass:[RZDSetCard class]]) {
            RZDSetCard *setCard = (RZDSetCard *)card;
            RZDSetCardView *setCardView = [[RZDSetCardView alloc] initWithFrame:rect];
            setCardView.rank = setCard.rank;
            setCardView.color = setCard.color;
            setCardView.shading = setCard.shading;
            setCardView.symbol = setCard.symbol;
            setCardView.faceUp = NO;
            setCardView.opaque = NO;
            [view addSubview:setCardView];
        }
    }
}

- (void)updateFoundMatchesViewCell:(UICollectionViewCell *)cell matchedCards:(NSArray *)matchedCards
{
    CGRect rect = CGRectMake(0, 0, 60, 85);
    for (id card in matchedCards) {
        if ([card isKindOfClass:[RZDSetCard class]]) {
            RZDSetCard *setCard = (RZDSetCard *)card;
            RZDSetCardView *setCardView = [[RZDSetCardView alloc] initWithFrame:rect];
            setCardView.rank = setCard.rank;
            setCardView.symbol = setCard.symbol;
            setCardView.color = setCard.color;
            setCardView.shading = setCard.shading;
            setCardView.faceUp = NO;
            setCardView.opaque = NO;
            [cell addSubview:setCardView];
            rect.origin.x += 20;
        }
    }
}


@end
