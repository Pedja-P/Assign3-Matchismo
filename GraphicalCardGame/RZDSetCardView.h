//
//  RZDSetCardView.h
//  GraphicalCardGame
//
//  Created by Predrag Pavlovic on 4/7/13.
//  Copyright (c) 2013 Rezedo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RZDSetCardView : UIView

@property (nonatomic) NSUInteger symbol;
@property (nonatomic) NSUInteger color;
@property (nonatomic) NSUInteger shading;
@property (nonatomic) NSUInteger rank;
@property (nonatomic) BOOL faceUp;

@end
