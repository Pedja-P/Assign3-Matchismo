//
//  RZDGameResult.h
//  GraphicalCardGame
//
//  Created by Predrag Pavlovic on 3/9/13.
//  Copyright (c) 2013 Rezedo. All rights reserved.
//  This class is not part of the Model, but it could be
//  (with different implementation, we could ask our game for result).
//  It is GameResultViewController's helper class.
//

#import <Foundation/Foundation.h>

@interface RZDGameResult : NSObject

+ (NSArray *)allGameResults;  // of GameResult
+ (NSString *)gameSetName;
+ (NSString *)gameMatchName;
+ (NSArray *)validGameNames;

- (instancetype)initWithGameName:(NSString *)name; // convenience initilizer

@property (strong, nonatomic, readonly) NSDate *start;
@property (strong, nonatomic, readonly) NSDate *end;
@property (strong, nonatomic, readonly) NSString *name;
@property (nonatomic, readonly) NSTimeInterval duration;
@property (nonatomic) int score;

- (NSComparisonResult)compareScoreToGameResult:(RZDGameResult *)otherResult;
- (NSComparisonResult)compareEndDateToGameResult:(RZDGameResult *)otherResult;
- (NSComparisonResult)compareDurationToGameResult:(RZDGameResult *)otherResult;

@end
