//
//  RZDCardGameViewController.m
//  GraphicalCardGame
//
//  Created by Predrag Pavlovic on 2/1/13.
//  Copyright (c) 2013 Rezedo. All rights reserved.
//

#import "RZDCardGameViewController.h"
#import "RZDCardMatchingGame.h"
#import "RZDGameResult.h"
#import "RZDGameSettings.h"
#import "CardCollectionHeaderView.h"

@interface RZDCardGameViewController () <UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout>

@property (weak, nonatomic) IBOutlet UILabel *flipsLabel;
@property (nonatomic) int flipCount;
@property (strong, nonatomic) RZDCardMatchingGame *game;
@property (weak, nonatomic) IBOutlet UILabel *scoreLabel;
@property (weak, nonatomic) IBOutlet UILabel *flipResultLabel;
@property (weak, nonatomic) IBOutlet UISegmentedControl *gameModeControl;
@property (strong, nonatomic) NSMutableArray *flipResultHistory; // of NSString
@property (weak, nonatomic) IBOutlet UISlider *flipResultHistorySlider; // not used for assn 3
@property (strong, nonatomic) RZDGameResult *gameResult;
@property (strong, nonatomic) RZDGameSettings *gameSettings;
@property (weak, nonatomic) IBOutlet UICollectionView *cardCollectionView;  // this is not necessary always
@property (weak, nonatomic) IBOutlet UIView *flipResultView;
@property (weak, nonatomic) IBOutlet UIButton *drawCardsButton;
@property (strong, nonatomic) NSMutableArray *foundMatches; // of NSArray of Cards

@end

@implementation RZDCardGameViewController

#define MUST_OVERRIDE() @throw [NSException exceptionWithName:NSInvalidArgumentException \
reason:[NSString stringWithFormat:@"%s must be overridden in a subclass/category", __PRETTY_FUNCTION__] userInfo:nil]

#pragma mark - Properties

- (RZDCardMatchingGame *)game
{
    if (!_game) {
        _game = [[RZDCardMatchingGame alloc] initWithCardCount:self.startingCardCount
                                                     usingDeck:[self createDeck]
                                                      gameMode:[self gameMode]];
    }
    return _game;
}

- (RZDDeck *)createDeck    // abstract
{
    MUST_OVERRIDE();    // it could be - return nil
}

- (NSUInteger)gameMode  // abstract
{
    MUST_OVERRIDE();
}

- (RZDGameResult *)gameResult
{
    if (!_gameResult) _gameResult =  [[RZDGameResult alloc] initWithGameName:[self gameName]];
    return _gameResult;
}

- (NSString *)gameName  // abstract
{
    MUST_OVERRIDE();
}

- (RZDGameSettings *)gameSettings
{
    if (!_gameSettings) _gameSettings =  [RZDGameSettings sharedGameSettings];
    return _gameSettings;
}

- (void)setFlipCount:(int)flipCount
{
    _flipCount = flipCount;
    self.flipsLabel.text = [NSString stringWithFormat:@"Flips: %d", self.flipCount];
}

- (NSMutableArray *)foundMatches
{
    if (!_foundMatches) {
        _foundMatches = [[NSMutableArray alloc] init];
    }
    return _foundMatches;
}

- (NSUInteger)startingCardCount // abstract
{
    MUST_OVERRIDE();
}

#pragma mark - UICollectionViewDataSource

// optional method in protocol, if not implemented will return 1
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 3;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView
     numberOfItemsInSection:(NSInteger)section
{
    NSInteger numberOfItems = 0;
    if (section == 0) {
        numberOfItems = [self.game cardCount];
    } else if (section == 2) {
        numberOfItems = [self.foundMatches count];
    } else {
        numberOfItems = [self.foundMatches count] ? 1 : 0;
    }
    return numberOfItems;
}

// providing data for UICollectionView
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView
                  cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell;
    if (indexPath.section == 0) {
        // NSIndexPath has properties section and item
        cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"Card" forIndexPath:indexPath];
        RZDCard *card = [self.game cardAtIndex:indexPath.item];
        [self updateCell:cell usingCard:card animated:NO];  // we need to update cells because it would reuse some that went off screen
    } else if (indexPath.section == 2) {
        cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"MatchedCards" forIndexPath:indexPath];
        [self updateFoundMatchesViewCell:cell matchedCards:self.foundMatches[indexPath.item]];
    } else {
        cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"Header" forIndexPath:indexPath];
        UILabel *textLabel = [[UILabel alloc] initWithFrame:cell.bounds];
        textLabel.text = @"Matched cards:";
        textLabel.backgroundColor = [UIColor clearColor];
        textLabel.font = [UIFont fontWithName:@"System Bold" size:20.0];
        UIView *subView = [[cell subviews] lastObject];
        // zato sto mrljavi text ako ga ne obrisemo pre toga, sigurno moze i na drugaciji nacin
        if (subView) {
            [subView removeFromSuperview];
        }
        [cell addSubview:textLabel];
    }
    return cell;
}

#pragma mark

- (void)updateCell:(UICollectionViewCell *)cell usingCard:(RZDCard *)card animated:(BOOL)animated
{
    MUST_OVERRIDE();
}

- (NSMutableArray *)flipResultHistory
{
    if (!_flipResultHistory) {
        _flipResultHistory = [[NSMutableArray alloc] init];
    }
    return _flipResultHistory;
}

- (void)updateUI
{
    for (UICollectionViewCell *cell in [self.cardCollectionView visibleCells]) {
        NSIndexPath *indexPath = [self.cardCollectionView indexPathForCell:cell];
        if (indexPath.section == 0) {
            RZDCard *card = [self.game cardAtIndex:indexPath.item];
            [self updateCell:cell usingCard:card animated:YES];
        }
    }
    self.scoreLabel.text = [NSString stringWithFormat:@"Score: %d", self.game.score];
    self.flipResultLabel.attributedText = [self.flipResultHistory lastObject];
    [self updateFlipResultView];
//    self.flipResultLabel.attributedText = [self.flipResultHistory count] ?
//        self.flipResultHistory[(int)(self.flipResultHistorySlider.value)] : @"";
//    self.flipResultLabel.alpha =
//        (self.flipResultHistorySlider.value == self.flipResultHistorySlider.maximumValue) ? 1.0 : 0.3;
}

// providing data for header (not used because of Apple's bug, we use 3rd cell instead
/*
- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView
           viewForSupplementaryElementOfKind:(NSString *)kind
                                 atIndexPath:(NSIndexPath *)indexPath
{
    UICollectionReusableView *reusableView = nil;
    
    if (kind == UICollectionElementKindSectionHeader) {
        reusableView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader
                                                          withReuseIdentifier:@"Header"
                                                                 forIndexPath:indexPath];
        if ([reusableView isKindOfClass:[CardCollectionHeaderView class]]) {
            CardCollectionHeaderView *headerView = (CardCollectionHeaderView *)reusableView;
            NSString *title;
            if (indexPath.section == 0) {
                title = @"Game";
            } else {
                title = @"Matched";
            }
            headerView.headerLabel.text = title;
            UIImage *image = [UIImage imageNamed:@"blue.jpg"];
            headerView.headerImage.image = image;
            reusableView = headerView;
        }
    }
    return reusableView;
}
*/

- (void)updateFlipResultHistory
{
    [self.flipResultHistory addObject:[self convertResultStringToAttributed:[self createTextForFlipResult]
                                                               matchedCards:self.game.matchedCards]];
//    self.flipResultHistorySlider.maximumValue = [self.flipResultHistory count] - 1;
//    self.flipResultHistorySlider.value = self.flipResultHistorySlider.maximumValue;
}

- (NSString *)createTextForFlipResult
{
    /*
    NSString *stringForFlipResult = @"";
    if ([self.game.flipResult isEqualToString:@"flipped up"]) {
        stringForFlipResult = [NSString stringWithFormat:@"Flipped up %@", self.game.matchedCards[0]];
    } else if ([self.game.flipResult isEqualToString:@"flipped down"]) {
        stringForFlipResult = [NSString stringWithFormat:@"Flipped down %@", self.game.matchedCards[0]];
    } else if ([self.game.flipResult isEqualToString:@"matched"]) {
        stringForFlipResult = [NSString stringWithFormat:@"Matched %@ for %d points",
                             [self.game.matchedCards componentsJoinedByString:@" & "], self.game.flipPoints];
    } else if ([self.game.flipResult isEqualToString:@"no match"]) {
        stringForFlipResult = [NSString stringWithFormat:@"%@ don't match! %d points",
                             [self.game.matchedCards componentsJoinedByString:@" & "], self.game.flipPoints];
    }
    return stringForFlipResult;
     */
    
    NSString *stringForFlipResult = @"";
    if ([self.game.flipResult isEqualToString:@"flipped up"]) {
        stringForFlipResult = [NSString stringWithFormat:@"Flipped up"];
    } else if ([self.game.flipResult isEqualToString:@"flipped down"]) {
        stringForFlipResult = [NSString stringWithFormat:@"Flipped down"];
    } else if ([self.game.flipResult isEqualToString:@"matched"]) {
        stringForFlipResult = [NSString stringWithFormat:@"Match! %d points",
                               self.game.flipPoints];
    } else if ([self.game.flipResult isEqualToString:@"no match"]) {
        stringForFlipResult = [NSString stringWithFormat:@"No match! %d points",
                               self.game.flipPoints];
    }
    return stringForFlipResult;
}

- (NSAttributedString *)convertResultStringToAttributed:(NSString *)resultString
                                           matchedCards:(NSArray *)matchedCards // abstract
{
    MUST_OVERRIDE();
}

- (IBAction)flipCard:(UITapGestureRecognizer *)gesture
{
    CGPoint tapLocation = [gesture locationInView:self.cardCollectionView];
    NSIndexPath *indexPath = [self.cardCollectionView indexPathForItemAtPoint:tapLocation];
    // only if card in section 0 is tapped and it is playable
    if (indexPath && (indexPath.section == 0) && ![self.game cardAtIndex:indexPath.item].isUnplayable) {
        [self.game flipCardAtIndex:indexPath.item];
        self.flipCount++;
        self.gameModeControl.enabled = NO;
        [self updateFlipResultHistory];
        if ([self.game.flipResult isEqualToString:@"matched"]) {
            [self updateFoundMatches];
            [self insertMatchInCollectionView];
            if (self.deletionOfCardsEnabled) {
                [self deleteCardsFromCollection];
            }
        }
        [self updateUI];
        self.gameResult.score = self.game.score;
    }
}

- (void)deleteCardsFromCollection
{
    NSIndexSet *indexSet = [self.game indexesOfCards:self.game.matchedCards];
    [self.game removeCardsAtIndexes:indexSet];
    NSArray *indexPaths = [self indexPathsFromIndexSet:indexSet];
    [self.cardCollectionView deleteItemsAtIndexPaths:indexPaths];
}

- (NSArray *)indexPathsFromIndexSet:(NSIndexSet *)indexSet
{
    NSMutableArray *indexPaths = [[NSMutableArray alloc] init];
    if (indexSet) {
        NSUInteger currentIndex = [indexSet firstIndex];
        while (currentIndex != NSNotFound) {
            [indexPaths addObject:[NSIndexPath indexPathForItem:currentIndex inSection:0]];
            currentIndex = [indexSet indexGreaterThanIndex:currentIndex];
        }
    }
    return indexPaths;
}

- (IBAction)dealCards
{
    self.game = nil;
    self.flipCount = 0;
    self.gameModeControl.enabled = YES;
    self.flipResultHistory = nil;
    self.flipResultHistorySlider.maximumValue = 0;
    self.gameResult = nil;
    self.foundMatches = nil;
    [self.cardCollectionView reloadData];
    self.drawCardsButton.enabled = YES;
    self.drawCardsButton.alpha = 1;
    [self updateUI];
}

- (IBAction)selectGameMode
{
    self.game = nil;
    [self updateUI];
    // so game could be initialized with selected game mode
}

- (IBAction)sliderMoved
{
    [self updateUI];
}

// shows or hide flip count label
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.flipsLabel.hidden = !self.gameSettings.isFlipCountEnabled;
}

/* used delegate methof instead
#define TOP_INSET 10
#define LEFT_INSET 0
#define BOTTOM_INSET 10
#define RIGHT_INSET 0
- (void)viewDidLoad
{
    [super viewDidLoad];
    // adding space between sections
    UICollectionViewFlowLayout *collectionViewLayout = (UICollectionViewFlowLayout *)self.cardCollectionView.collectionViewLayout;
    collectionViewLayout.sectionInset = UIEdgeInsetsMake(TOP_INSET, LEFT_INSET, BOTTOM_INSET, RIGHT_INSET);
}
 */

#define NUMBER_OF_CARDS_TO_DRAW 3
#define DRAW_BUTTON_DISABLED_ALPHA 0.5
- (IBAction)drawCards:(UIButton *)sender
{
    NSUInteger numberOfCardsInGame = [self.game cardCount];
    BOOL cardsLeftInDeck = [self.game addCards:NUMBER_OF_CARDS_TO_DRAW];
    NSUInteger numberOfAddedCards = [self.game cardCount] - numberOfCardsInGame;
    if (numberOfAddedCards) {
        [self insertCardsInCollection:numberOfAddedCards];
    }
    if (!cardsLeftInDeck) {
        sender.enabled = NO;
        sender.alpha = DRAW_BUTTON_DISABLED_ALPHA;
    }
}

// inserts cards at the end of collection view
- (void)insertCardsInCollection:(NSUInteger)numberOfCards
{
    NSUInteger numberOfItems = [self.cardCollectionView numberOfItemsInSection:0];
    NSMutableArray *indexPaths = [[NSMutableArray alloc] init];
    for (unsigned i = numberOfItems; i < numberOfItems + numberOfCards; ++i) {
        [indexPaths addObject:[NSIndexPath indexPathForItem:i inSection:0]];
    }
    [self.cardCollectionView insertItemsAtIndexPaths:indexPaths];
    [self.cardCollectionView scrollToItemAtIndexPath:[indexPaths lastObject]
                                     atScrollPosition:UICollectionViewScrollPositionBottom
                                             animated:YES];
}

#define CARD_WIDTH_TO_HEIGHT_RATIO 0.7
- (void)updateFlipResultView
{
    CGFloat cardHeight = self.flipResultView.bounds.size.height;
    CGFloat cardWidth = cardHeight * CARD_WIDTH_TO_HEIGHT_RATIO;
    NSUInteger numberOfCardsInView = [self gameMode] + 1;
    
    // spacing between cards and before/after first/last card is the same
    CGFloat cardXOffset = (self.flipResultView.bounds.size.width - numberOfCardsInView * cardWidth) / (numberOfCardsInView + 1);
    CGFloat yOrigin = 0.0;
    CGFloat xOrigin = 0.0;
    CGRect rect = CGRectMake(xOrigin + cardXOffset, yOrigin, cardWidth, cardHeight);
    for (UIView *subView in [self.flipResultView subviews]) {
        [subView removeFromSuperview];
    }
    if (![self.game.flipResult isEqualToString:@"flipped down"]) {
        for (RZDCard *card in self.game.matchedCards) {
            [self addCard:card resultView:self.flipResultView inRect:rect];
            rect.origin.x = rect.origin.x + cardWidth + cardXOffset;
        }
    }
}

- (void)addCard:(RZDCard *)card resultView:(UIView *)view inRect:(CGRect)rect    // abstract
{
    
}

- (void)updateFoundMatches
{
    [self.foundMatches addObject:self.game.matchedCards];
}

- (void)updateFoundMatchesViewCell:(UICollectionViewCell *)cell matchedCards:(NSArray *)matchedCards    // abstract
{
    
}

- (void)insertMatchInCollectionView
{
    // there is a bug when inserting first element into section, so in that case we should reload data,
    // but it is, I think, only if there is header/footer
    
    NSMutableArray *indexPaths = [[NSMutableArray alloc] init];
    [indexPaths addObject:[NSIndexPath indexPathForItem:[self.cardCollectionView numberOfItemsInSection:2] inSection:2]];
    /* it could be done like this too:
    if ([self.foundMatches count] == 1) {
        [indexPaths addObject:[NSIndexPath indexPathForItem:0 inSection:1]];
    }
    [self.cardCollectionView insertItemsAtIndexPaths:indexPaths];
     */
        [self.cardCollectionView performBatchUpdates:^{
        [self.cardCollectionView insertItemsAtIndexPaths:indexPaths];
        if ([self.foundMatches count] == 1) {
            [self.cardCollectionView insertItemsAtIndexPaths:@[[NSIndexPath indexPathForItem:0 inSection:1]]];
        }
    } completion:nil];
}

#pragma mark - UICollectionViewDelegateFlowLayout

// delegate method (UICollectionViewDelegateFlowLayout), sets size of cells in sections
- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout*)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGSize cellSize;
    if (indexPath.section == 1) cellSize = CGSizeMake(150, 20);
    if (indexPath.section == 0) cellSize = CGSizeMake(70, 100);
    if (indexPath.section == 2) cellSize = CGSizeMake(100, 85);
    return cellSize;
}

// delegate method (UICollectionViewDelegateFlowLayout), sets insets of cells in sections
- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView
                        layout:(UICollectionViewLayout*)collectionViewLayout
        insetForSectionAtIndex:(NSInteger)section
{
    if (section == 2) return UIEdgeInsetsMake(20, 0, 0, 0);
    if (section == 1) return UIEdgeInsetsMake(20, 5, 0, 0);
    return UIEdgeInsetsMake(0, 0, 0, 0);
}

@end
