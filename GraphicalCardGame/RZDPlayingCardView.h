//
//  RZDPlayingCardView.h
//  GraphicalCardGame
//
//  Created by Predrag Pavlovic on 3/24/13.
//  Copyright (c) 2013 Rezedo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RZDPlayingCardView : UIView

@property (nonatomic) NSUInteger rank;
@property (copy, nonatomic) NSString *suit;
@property (nonatomic, getter = isFaceUp) BOOL faceUp;

- (void)pinch:(UIPinchGestureRecognizer *)gesture;

@end
