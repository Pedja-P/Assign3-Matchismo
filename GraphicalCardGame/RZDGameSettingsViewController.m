//
//  RZDGameSettingsViewController.m
//  GraphicalCardGame
//
//  Created by Predrag Pavlovic on 3/15/13.
//  Copyright (c) 2013 Rezedo. All rights reserved.
//

#import "RZDGameSettingsViewController.h"
#import "RZDGameSettings.h"

@interface RZDGameSettingsViewController () <UIPickerViewDataSource, UIPickerViewDelegate>

@property (weak, nonatomic) IBOutlet UILabel *showFlipCountLabel;
@property (weak, nonatomic) IBOutlet UISwitch *showFlipCountSwitch;
@property (weak, nonatomic) IBOutlet UILabel *cardNumberLabel;
@property (weak, nonatomic) IBOutlet UIPickerView *cardNumberPicker;
@property (strong, nonatomic) NSArray *cardCounts;
@property (strong, nonatomic) RZDGameSettings *settings;

@end

@implementation RZDGameSettingsViewController

// settings getter (only one object can be initialized)
- (RZDGameSettings *)settings
{
    if (!_settings) {
        _settings = [RZDGameSettings sharedGameSettings];
    }
    return _settings;
}

- (NSArray *)cardCounts
{
    if (!_cardCounts) {
        NSMutableArray *mutableCardCounts = [[NSMutableArray alloc] init];
        for (unsigned i = 22; i <= 52; ++i) {
            [mutableCardCounts addObject:@(i)];
            _cardCounts = [mutableCardCounts copy];
        }
    }
    return _cardCounts;
}

// switch moved
- (IBAction)showFlipCountSwitched {
    self.settings.flipCountEnabled = self.showFlipCountSwitch.isOn;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self updateUI];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self.settings synchronize];
}

// sets switch position to match saved in standardUserDefaults
- (void)updateUI
{
    self.showFlipCountSwitch.on = self.settings.isFlipCountEnabled;
    [self.cardNumberPicker selectRow:[self.cardCounts indexOfObject:@(self.settings.startingCardCount)] inComponent:0 animated:NO];
}

#pragma mark - PickerView datasource methods

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return 31;
}

#pragma mark - PickerView delegate methods

- (NSString *)pickerView:(UIPickerView *)pickerView
             titleForRow:(NSInteger)row
            forComponent:(NSInteger)component
{
    return [[self.cardCounts objectAtIndex:row] description];   // NSString from NSUInteger
}

- (void)pickerView:(UIPickerView *)pickerView
      didSelectRow:(NSInteger)row
       inComponent:(NSInteger)component
{
    self.settings.startingCardCount = [[self.cardCounts objectAtIndex:row] unsignedIntegerValue];
}

#pragma mark - (Unused) Initialization before viewDidLoad

- (void)setup
{
    // initialization that can't wait until viewDidLoad
}

- (void)awakeFromNib
{
    [self awakeFromNib];
    [self setup];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    [self setup];
    return self;
}

@end
