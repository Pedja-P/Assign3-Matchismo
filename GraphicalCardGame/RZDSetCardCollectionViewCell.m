//
//  RZDSetCardCollectionView.m
//  GraphicalCardGame
//
//  Created by Predrag Pavlovic on 4/7/13.
//  Copyright (c) 2013 Rezedo. All rights reserved.
//

#import "RZDSetCardCollectionViewCell.h"

@implementation RZDSetCardCollectionViewCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
