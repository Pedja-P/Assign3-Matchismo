//
//  RZDGameResultViewController.m
//  GraphicalCardGame
//
//  Created by Predrag Pavlovic on 3/9/13.
//  Copyright (c) 2013 Rezedo. All rights reserved.
//

#import "RZDGameResultViewController.h"
#import "RZDGameResult.h"

@interface RZDGameResultViewController ()

@property (weak, nonatomic) IBOutlet UITextView *matchGameDisplay;
@property (weak, nonatomic) IBOutlet UITextView *setGameDisplay;
@property (nonatomic) SEL sortSelector; 

@end

@implementation RZDGameResultViewController

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self updateUI];
}

- (void)updateUI
{
    NSString *matchDisplayText = @"";
    NSString *setDisplayText = @"";
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateStyle:NSDateFormatterShortStyle];
    [formatter setTimeStyle:NSDateFormatterShortStyle];
    for (RZDGameResult *result in [[RZDGameResult allGameResults]
                                sortedArrayUsingSelector:self.sortSelector]) { // sorted
        if ([result.name isEqualToString:[RZDGameResult gameMatchName]]) {
            matchDisplayText = [matchDisplayText stringByAppendingFormat:@"%@: Score: %d (%@, %0gs)\n",
                                result.name, result.score, [formatter stringFromDate:result.end],
                                round(result.duration)];  // formatted date
        } else if ([result.name isEqualToString:[RZDGameResult gameSetName]]) {
            setDisplayText = [setDisplayText stringByAppendingFormat:@"%@: Score: %d (%@, %0gs)\n",
                                result.name, result.score, [formatter stringFromDate:result.end],
                                round(result.duration)];  // formatted date
        }
    }
    self.matchGameDisplay.text = matchDisplayText;
    self.setGameDisplay.text = setDisplayText;
}

#pragma mark - Sorting

@synthesize sortSelector = _sortSelector; 

// returns default sort selector if none set (by score)
- (SEL)sortSelector
{
    if (!_sortSelector) _sortSelector = @selector(compareScoreToGameResult:);
    return _sortSelector;
}

// sortSelector setter, updates the UI when changing the sort selector
- (void)setSortSelector:(SEL)sortSelector
{
    _sortSelector = sortSelector;
    [self updateUI];
}

- (IBAction)sortByDate
{
    self.sortSelector = @selector(compareEndDateToGameResult:);
}

- (IBAction)sortByScore
{
    self.sortSelector = @selector(compareScoreToGameResult:);
}

- (IBAction)sortByDuration
{
    self.sortSelector = @selector(compareDurationToGameResult:);
}

#pragma mark - (Unused) Initialization before viewDidLoad

- (void)setup
{
    // initialization that can't wait until viewDidLoad
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    [self setup];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    [self setup];
    return self;
}

@end
