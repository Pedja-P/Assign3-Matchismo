//
//  RZDPlayingCardGameViewController.m
//  GraphicalCardGame
//
//  Created by Predrag Pavlovic on 3/31/13.
//  Copyright (c) 2013 Rezedo. All rights reserved.
//

#import "RZDPlayingCardGameViewController.h"
#import "RZDPlayingCardDeck.h"
#import "RZDPlayingCard.h"
#import "RZDPlayingCardCollectionViewCell.h"
#import "RZDGameResult.h"
#import "RZDGameSettings.h"

@interface RZDPlayingCardGameViewController ()

@end

@implementation RZDPlayingCardGameViewController

- (NSUInteger)startingCardCount
{
    return ((RZDGameSettings *)[RZDGameSettings sharedGameSettings]).startingCardCount;
}

- (NSString *)gameName
{
    return [RZDGameResult gameMatchName];
}

- (RZDDeck *)createDeck
{
    return [[RZDPlayingCardDeck alloc] init];
}

#define MATCH_GAME_MODE 1   // 2 card matching game
- (NSUInteger)gameMode
{
    return MATCH_GAME_MODE;
}

#define UNPLAYABLE_CARD_ALPHA 0.3
#define PLAYABLE_CARD_ALPHA 1
- (void)updateCell:(UICollectionViewCell *)cell usingCard:(RZDCard *)card animated:(BOOL)animated
{
    if ([cell isKindOfClass:[RZDPlayingCardCollectionViewCell class]]) {
        RZDPlayingCardView *playingCardView = ((RZDPlayingCardCollectionViewCell *)cell).playingCardView;
        if ([card isKindOfClass:[RZDPlayingCard class]]) {
            RZDPlayingCard *playingCard = (RZDPlayingCard *)card;
            playingCardView.rank = playingCard.rank;
            playingCardView.suit = playingCard.suit;
            playingCardView.alpha = playingCard.isUnplayable ? UNPLAYABLE_CARD_ALPHA : PLAYABLE_CARD_ALPHA;
            if (playingCardView.faceUp != playingCard.isFaceUp) {
                if (animated) {
                    [UIView transitionWithView:playingCardView
                                      duration:0.5
                                       options:UIViewAnimationOptionTransitionFlipFromLeft
                                    animations:^{
                                        playingCardView.faceUp = playingCard.isFaceUp;
                                    } completion:NULL];
                } else {
                    playingCardView.faceUp = playingCard.isFaceUp;
                }
            }
        }
    }
}

#define RESULT_STRING_FONT_SIZE 13.0
- (NSAttributedString *)convertResultStringToAttributed:(NSString *)resultString
                                           matchedCards:(NSArray *)matchedCards;
{
    NSMutableAttributedString *mat = [[NSMutableAttributedString alloc] initWithString:resultString];
    [mat addAttribute:NSFontAttributeName
                value:[UIFont systemFontOfSize:RESULT_STRING_FONT_SIZE]
                range:NSMakeRange(0, [mat length])];
    return mat;
}

// adds card view to flip result view
- (void)addCard:(RZDCard *)card resultView:(UIView *)view inRect:(CGRect)rect
{
    if (card) {
        if ([card isKindOfClass:[RZDPlayingCard class]]) {
            RZDPlayingCard *playingCard = (RZDPlayingCard *)card;
            RZDPlayingCardView *playingCardView = [[RZDPlayingCardView alloc] initWithFrame:rect];
            playingCardView.rank = playingCard.rank;
            playingCardView.suit = playingCard.suit;
            playingCardView.faceUp = YES;
            playingCardView.opaque = NO;
            [view addSubview:playingCardView];
        }
    }
}

- (void)updateFoundMatchesViewCell:(UICollectionViewCell *)cell matchedCards:(NSArray *)matchedCards
{
    CGRect rect = CGRectMake(0, 0, 60, 85);
    for (id card in matchedCards) {
        if ([card isKindOfClass:[RZDPlayingCard class]]) {
            RZDPlayingCard *playingCard = (RZDPlayingCard *)card;
            RZDPlayingCardView *playingCardView = [[RZDPlayingCardView alloc] initWithFrame:rect];
            playingCardView.rank = playingCard.rank;
            playingCardView.suit = playingCard.suit;
            playingCardView.faceUp = YES;
            playingCardView.opaque = NO;
            [cell addSubview:playingCardView];
            rect.origin.x += 20;
        }
    }
}

@end
