//
//  main.m
//  GraphicalCardGame
//
//  Created by Predrag Pavlovic on 3/30/13.
//  Copyright (c) 2013 Rezedo. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "RZDGraphicalCardGameAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([RZDGraphicalCardGameAppDelegate class]));
    }
}
