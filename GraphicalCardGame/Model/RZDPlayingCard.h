//
//  RZDPlayingCard.h
//  GraphicalCardGame
//
//  Created by Predrag Pavlovic on 2/1/13.
//  Copyright (c) 2013 Rezedo. All rights reserved.
//

#import "RZDCard.h"

@interface RZDPlayingCard : RZDCard

+ (NSArray *)validSuits;
+ (NSUInteger)maxRank;

@property (copy, nonatomic) NSString *suit;
@property (nonatomic) NSUInteger rank;

@end
