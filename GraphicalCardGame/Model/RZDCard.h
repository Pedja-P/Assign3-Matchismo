//
//  RZDCard.h
//  GraphicalCardGame
//
//  Created by Predrag Pavlovic on 2/1/13.
//  Copyright (c) 2013 Rezedo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RZDCard : NSObject

@property (copy, nonatomic) NSString *contents;
@property (nonatomic, getter = isFaceUp) BOOL faceUp;
@property (nonatomic, getter = isUnplayable) BOOL unplayable;

- (int)match:(NSArray *)otherCards; // of Cards

@end
