//
//  RZDSetCardDeck.m
//  GraphicalCardGame
//
//  Created by Predrag Pavlovic on 3/9/13.
//  Copyright (c) 2013 Rezedo. All rights reserved.
//

#import "RZDSetCardDeck.h"
#import "RZDSetCard.h"

@implementation RZDSetCardDeck

// designated initializer
- (id)init
{
    self = [super init];
    if (self) {
        for (NSUInteger i = 1; i <= [RZDSetCard maxNumber]; ++i) {
            for (NSNumber *color in [RZDSetCard validColors]) {
                for (NSNumber *symbol in [RZDSetCard validSymbols]) {
                    for (NSNumber *shading in [RZDSetCard validShadings]) {
                        RZDSetCard *card = [[RZDSetCard alloc] init];
                        card.rank = i;
                        card.color = [color intValue];
                        card.symbol = [symbol intValue];
                        card.shading = [shading intValue];
                        [self addCard:card atTop:YES];
                    }
                }
            }
        }
    }
    return self;
}

@end
