//
//  RZDDeck.h
//  GraphicalCardGame
//
//  Created by Predrag Pavlovic on 2/1/13.
//  Copyright (c) 2013 Rezedo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RZDCard.h"

@interface RZDDeck : NSObject

- (void)addCard:(RZDCard *)aCard atTop:(BOOL)atTop;
- (RZDCard *)drawRandomCard;
- (NSUInteger)count;

@end
