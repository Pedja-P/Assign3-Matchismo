//
//  RZDPlayingCard.m
//  GraphicalCardGame
//
//  Created by Predrag Pavlovic on 2/1/13.
//  Copyright (c) 2013 Rezedo. All rights reserved.
//

#import "RZDPlayingCard.h"

@implementation RZDPlayingCard

// we implemented setter and getter
@synthesize suit = _suit;

// overrides Card match implementation
- (int)match:(NSArray *)otherCards
{
    int score = 0;
    unsigned suitMatch = 0;
    unsigned rankMatch = 0;
    
    if ([otherCards count]) {
        for (id otherCard in otherCards) {
            // we can only match playing cards by suit and rank
            if ([otherCard isKindOfClass:[RZDPlayingCard class]]) {
                RZDPlayingCard *otherPlayingCard = (RZDPlayingCard *)otherCard;
                if ([otherPlayingCard.suit isEqualToString:self.suit]) {
                    suitMatch++;
                } else if (otherPlayingCard.rank == self.rank) {
                    rankMatch++;
                }
            }
        }
        if (suitMatch == otherCards.count) {
            score = 1 * otherCards.count;
        } else if (rankMatch == otherCards.count) {
            score = 4 * otherCards.count;
        }
    }
    return score;
}

// overrides Card's contents getter
- (NSString *)contents
{
    NSArray *rankStrings = [[self class] rankStrings];
    return [rankStrings[self.rank] stringByAppendingString:self.suit];
}

// returns valid suits for playing card
+ (NSArray *)validSuits
{
    /* 
     * it can be done like this
     return @[@"♥", @"♦", @"♠", @"♣"];
     * or
     */
    static NSArray *validSuits = nil;
    if (!validSuits) validSuits = @[@"♥", @"♦", @"♠", @"♣"];
    return validSuits;
    // C static validSuits, only created and initialized once
    // it can not be done like
    // static NSArray *validSuits = @[@"♥", @"♦", @"♠", @"♣"];
    // because NSArray @[@"♥", @"♦", @"♠", @"♣"] is not const
}

// suit setter
- (void)setSuit:(NSString *)suit
{
    if ([[[self class] validSuits] containsObject:suit]) {  // set to valid suit only
        _suit = [suit copy];
    }
}

// suit getter
- (NSString *)suit
{
    return _suit ? _suit : @"?";
}

// class method, returns maximum rank
+ (NSUInteger)maxRank
{
    return [[self rankStrings] count] - 1;
}

// class method, returns array of card's ranks as string
+ (NSArray *)rankStrings
{
    /*
     * it can be done like this
     * return @[@"?", @"A", @"2", @"3", @"4", @"5", @"6", @"7", @"8", @"9", @"10",
     *        @"J", @"Q", @"K"];
     * or
     */
    static NSArray *rankStrings = nil;
    if (!rankStrings) {
        rankStrings = @[@"?", @"A", @"2", @"3", @"4", @"5", @"6", @"7", @"8", @"9",
                        @"10", @"J", @"Q", @"K"];
    }
    return rankStrings;
}

// rank setter
- (void)setRank:(NSUInteger)rank
{
    if (rank <= [[self class] maxRank]) { // set to valid rank only
        _rank = rank;
    }
}

@end
