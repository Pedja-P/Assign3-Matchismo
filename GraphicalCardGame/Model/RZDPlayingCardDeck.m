//
//  RZDPlayingCardDeck.m
//  GraphicalCardGame
//
//  Created by Predrag Pavlovic on 2/1/13.
//  Copyright (c) 2013 Rezedo. All rights reserved.
//

#import "RZDPlayingCardDeck.h"
#import "RZDPlayingCard.h"

@implementation RZDPlayingCardDeck

// designated initializer
- (id)init
{
    self = [super init];
    if (self) {
        for (NSString *suit in [RZDPlayingCard validSuits]) {
            for (NSUInteger rank = 1; rank <= [RZDPlayingCard maxRank]; rank++) {
                RZDPlayingCard *card = [[RZDPlayingCard alloc] init];
                card.rank = rank;
                card.suit = suit;
                [self addCard:card atTop:YES];
            }
        }
    }
    return self;
}

@end
