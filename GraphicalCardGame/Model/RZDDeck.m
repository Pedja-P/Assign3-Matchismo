//
//  RZDDeck.m
//  GraphicalCardGame
//
//  Created by Predrag Pavlovic on 2/1/13.
//  Copyright (c) 2013 Rezedo. All rights reserved.
//

#import "RZDDeck.h"

@interface RZDDeck ()

@property (strong, nonatomic) NSMutableArray *cards; // of Card

@end

@implementation RZDDeck

// cards getter
- (NSMutableArray *)cards // of Card
{
    if (!_cards) _cards = [[NSMutableArray alloc] init];
    return _cards;
}

// inserts card in deck
- (void)addCard:(RZDCard *)aCard atTop:(BOOL)atTop
{
    if (aCard) {
        if (atTop) {
            [self.cards insertObject:aCard atIndex:0];
        } else {
            [self.cards addObject:aCard];
        }
    }
}

// draws random card from deck
- (RZDCard *)drawRandomCard
{
    RZDCard *randomCard;
    if ([self.cards count]) {
        unsigned cardIndex = arc4random() % [self.cards count];
        randomCard = self.cards[cardIndex];
        [self.cards removeObjectAtIndex:cardIndex];
    }
    return randomCard;
}

// returns number of cards in deck
- (NSUInteger)count;
{
    return [self.cards count];
}

@end
