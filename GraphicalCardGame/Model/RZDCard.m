//
//  RZDCard.m
//  GraphicalCardGame
//
//  Created by Predrag Pavlovic on 2/1/13.
//  Copyright (c) 2013 Rezedo. All rights reserved.
//

#import "RZDCard.h"

@implementation RZDCard

// check if cards match
- (int)match:(NSArray *)otherCards  // of Cards
{
    int score = 0;
    for (RZDCard *card in otherCards) {
        if ([card.contents isEqualToString:self.contents]) {
            score = 1;
            break;
        }
    }
    return score;
}

// overrides NSObject's description
- (NSString *)description
{
    return self.contents;
}

@end
