//
//  RZDPlayingCardDeck.h
//  GraphicalCardGame
//
//  Created by Predrag Pavlovic on 2/1/13.
//  Copyright (c) 2013 Rezedo. All rights reserved.
//

#import "RZDDeck.h"

@interface RZDPlayingCardDeck : RZDDeck

@end
