//
//  RZDSetCard.h
//  GraphicalCardGame
//
//  Created by Predrag Pavlovic on 3/9/13.
//  Copyright (c) 2013 Rezedo. All rights reserved.
//

#import "RZDCard.h"

// "set card's" symbols, shading and color as enums
typedef NS_ENUM(NSUInteger, SetCardSymbol) {
    SetCardSymbolDefault = 0,
    SetCardSymbolOne,
    SetCardSymbolTwo,
    SetCardSymbolThree,
};

typedef NS_ENUM(NSUInteger, SetCardShading) {
    SetCardShadingDefault = 0,
    SetCardShadingOne,
    SetCardShadingTwo,
    SetCardShadingThree,
};

typedef NS_ENUM(NSUInteger, SetCardColor) {
    SetCardColorDefault = 0,
    SetCardColorOne,
    SetCardColorTwo,
    SetCardColorThree,
};


@interface RZDSetCard : RZDCard

+ (NSArray *)validSymbols;
+ (NSArray *)validShadings;
+ (NSArray *)validColors;
+ (NSUInteger)maxNumber;

@property (nonatomic) SetCardSymbol symbol;
@property (nonatomic) SetCardShading shading;
@property (nonatomic) SetCardColor color;
@property (nonatomic) NSUInteger rank;

@end
