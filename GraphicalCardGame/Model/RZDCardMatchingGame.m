//
//  RZDCardMatchingGame.m
//  GraphicalCardGame
//
//  Created by Predrag Pavlovic on 2/9/13.
//  Copyright (c) 2013 Rezedo. All rights reserved.
//

#import "RZDCardMatchingGame.h"

@interface RZDCardMatchingGame ()

@property (nonatomic, readwrite) int score;
@property (strong, nonatomic) NSMutableArray *cards;    // of Card
@property (strong, nonatomic) RZDDeck *deck;
@property (strong, nonatomic, readwrite) NSString *flipResult;
@property (strong, nonatomic, readwrite) NSArray *matchedCards; // of Card
@property (nonatomic) NSUInteger gameMode;  // 1 is 2 card match, 2 is 3 card
@property (nonatomic, readwrite) NSInteger flipPoints;
@property (nonatomic, readwrite) NSUInteger numberOfMatches;

@end

@implementation RZDCardMatchingGame

// matchedCards getter
- (NSArray *)matchedCards   // of Card
{
    if (!_matchedCards) {
        _matchedCards = [[NSArray alloc] init];
    }
    return _matchedCards;
}

- (NSUInteger)cardCount
{
    return [self.cards count];
}

// designated initializer
- (instancetype)initWithCardCount:(NSUInteger)count
              usingDeck:(RZDDeck *)deck
               gameMode:(NSUInteger)mode
{
    self = [super init];
    if (self) {
        _gameMode = mode;
        _deck = deck;
        for (unsigned i = 0; i < count; ++i) {
            RZDCard *card = [deck drawRandomCard];
            if (card) { // protects from insufficient number of cards in deck
                self.cards[i] = card;   // this calls the getter, not the setter
                // [[self cards] setObject:card atIndexedSubscript:i]
            } else {
                self = nil;
                break;
            }
        }
    }
    return self;    
}

// we can't initialize game with init, so we throw exception
- (id)init
{
    @throw [NSException exceptionWithName:@"CardMatchingGameInitialization"
                                   reason:[NSString stringWithFormat:@"Use initWithCardCount:usingDeck:andGameMode:, not %@", NSStringFromSelector(_cmd)]
                                 userInfo:nil];
}

// cards getter
- (NSMutableArray *)cards   // of Card
{
    if (!_cards) {
        _cards = [[NSMutableArray alloc] init];
    }
    return _cards;
}

// returns card at given index
- (RZDCard *)cardAtIndex:(NSUInteger)cardIndex
{
    // check to see if argument is out of bounds
    return (cardIndex < [self.cards count]) ? self.cards[cardIndex] : nil;
}

#define MATCH_BONUS 4
#define MISMATCH_PENALTY 2
#define FLIP_COST 1

// flips card at given index, determines match, score, flip result
- (void)flipCardAtIndex:(NSUInteger)cardIndex
{
    self.flipResult = @"";
    self.flipPoints = 0;
    RZDCard *card = [self cardAtIndex:cardIndex];
    NSMutableArray *mutableMatchedCards = [[NSMutableArray alloc] init];
    if (card && !card.isUnplayable) {   // there is a card and it's playable
        self.flipResult = @"flipped down";
        if (!card.isFaceUp) {   // it is not face up
            // see if flipping this card creates a match
            // loop through the other cards looking for other face up and playable cards
            self.flipPoints = -FLIP_COST;
            self.score += self.flipPoints;
            self.flipResult = @"flipped up";
            
            // create array with all other face up cards
            for (RZDCard *otherCard in self.cards) {
                if (otherCard.isFaceUp && !otherCard.isUnplayable) {
                    [mutableMatchedCards addObject:otherCard];
                    if ([mutableMatchedCards count] == self.gameMode) {
                        int matchScore = [card match:mutableMatchedCards];
                        if (matchScore) {
                            card.unplayable = YES;
                            for (RZDCard *matchedCard in mutableMatchedCards) {
                                matchedCard.unplayable = YES;
                            }
                            self.flipPoints = matchScore * MATCH_BONUS;
                            self.score += self.flipPoints;
                            self.flipResult = @"matched";
                        } else {
                            // turn mismatched cards face down, so there is always only one card facing up
                            for (RZDCard *matchedCard in mutableMatchedCards) {
                                matchedCard.faceUp = NO;
                            }
                            self.flipPoints = -MISMATCH_PENALTY;
                            self.score += self.flipPoints;
                            self.flipResult = @"no match";
                        }
                        break;
                    }
                }
            }
        }
       // [mutableMatchedCards insertObject:card atIndex:0];
        [mutableMatchedCards addObject:card];
        self.matchedCards = [mutableMatchedCards copy];
        card.faceUp = !card.faceUp; // flip card
    }
}

// deletes cards at indexes in indexSet
- (void)removeCardsAtIndexes:(NSIndexSet *)indexSet
{
    [self.cards removeObjectsAtIndexes:indexSet];
}

// returns the index set of the cards in passed array
- (NSIndexSet *)indexesOfCards:(NSArray *)selectedCards // of Card
{
    NSMutableIndexSet *indexSet = [[NSMutableIndexSet alloc] init];
    if (selectedCards) {
        for (id singleCard in selectedCards) {
            if ([singleCard isKindOfClass:[RZDCard class]]) {
                RZDCard *card = (RZDCard *)singleCard;
                [indexSet addIndex:[self.cards indexOfObject:card]];
            }
        }
    }
    return indexSet;
}

// adds numberOfCardsToAdd or all remaining cards to the game from deck, returns FALSE if no more cards in deck
- (BOOL)addCards:(NSUInteger)numberOfCardsToAdd
{
    NSUInteger numberOfCardsInDeck = [self.deck count];
    for (unsigned i = 0; (i < numberOfCardsToAdd) && (i < numberOfCardsInDeck); ++i) {
        [self.cards addObject:[self.deck drawRandomCard]];
    }
    BOOL cardsLeftInDeck = [self.deck count] > 0;
    return cardsLeftInDeck;
}

@end
