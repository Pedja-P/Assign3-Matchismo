//
//  RZDSetCard.m
//  GraphicalCardGame
//
//  Created by Predrag Pavlovic on 3/9/13.
//  Copyright (c) 2013 Rezedo. All rights reserved.
//

#import "RZDSetCard.h"

@implementation RZDSetCard

@synthesize rank = _rank;
@synthesize symbol = _symbol;
@synthesize shading = _shading;
@synthesize color = _color;

#pragma mark - Properties

// color getter
- (SetCardColor)color
{
    return _color ? _color : SetCardColorDefault;
}

// color setter
- (void)setColor:(SetCardColor)color
{
    if ([[[self class] validColors] containsObject:@(color)]) { // sets only to valid colors
        _color = color;
    }
}

// symbol getter
- (SetCardSymbol)symbol
{
    return _symbol ? _symbol : SetCardSymbolDefault;
}

// symbol setter
- (void)setSymbol:(SetCardSymbol)symbol
{
    if ([[[self class] validSymbols] containsObject:@(symbol)]) {   // only valid symbols
        _symbol = symbol;
    }
}

// shading getter
- (SetCardShading)shading
{
    return _shading ? _shading : SetCardShadingDefault;
}

// shading setter
- (void)setShadings:(SetCardShading)shading
{
    if ([[[self class] validShadings] containsObject:@(shading)]) { // only valid shadings allowed
        _shading = shading;
    }
}

#pragma mark - Overrided Methods

// overrides Card's match method
- (int)match:(NSArray *)otherCards 
{
    int score = 0;
    NSUInteger symbolSum = self.symbol;
    NSUInteger rankSum = self.rank;
    NSUInteger shadingSum = self.shading;
    NSUInteger colorSum = self.color;
    
    if ([otherCards count]) {
        for (id otherCard in otherCards) {
            if ([otherCard isKindOfClass:[RZDSetCard class]]) {
                RZDSetCard *otherSetCard = (RZDSetCard *)otherCard;
                symbolSum += otherSetCard.symbol;
                rankSum += otherSetCard.rank;
                shadingSum += otherSetCard.shading;
                colorSum += otherSetCard.color;
            } else {
                return 0;   // if any of the cards is not SetCard
            }
        }
        // rules of the game
        if ((symbolSum % 3 == 0) &&
            (rankSum % 3 == 0) &&
            (shadingSum % 3 == 0) &&
            (colorSum % 3 == 0)) {
            score = 3;
        }
    }
    return score;
}

// overrides Card's contents getter
- (NSString *)contents
{
    return [NSString stringWithFormat:@"[%u, %u, %u, %u]", self.rank, self.color,
            self.shading, self.symbol];
}

#pragma mark - Class Methods

// returns valid colors
+ (NSArray *)validColors
{
    static NSArray *validColors = nil;
    if (!validColors) {
        validColors = @[@(SetCardColorOne), @(SetCardColorTwo), @(SetCardColorThree)];
    }
    return validColors;
}

// returns valid symbols
+ (NSArray *)validSymbols
{
    static NSArray *validSymbols = nil;
    if (!validSymbols) {
        validSymbols = @[@(SetCardSymbolOne), @(SetCardSymbolTwo), @(SetCardSymbolThree)];
    }
    return validSymbols;
}

// returns valid shadings
+ (NSArray *)validShadings
{
    static NSArray *validShadings = nil;
    if (!validShadings) {
        validShadings = @[@(SetCardShadingOne), @(SetCardShadingTwo), @(SetCardShadingThree)];
    }
    return validShadings;
}

#define MAX_NUMBER 3  // maximum number of "shapes" on Set card is 3
+ (NSUInteger)maxNumber
{
    return MAX_NUMBER;
}

@end
