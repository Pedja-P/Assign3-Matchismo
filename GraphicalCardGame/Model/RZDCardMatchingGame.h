//
//  RZDCardMatchingGame.h
//  GraphicalCardGame
//
//  Created by Predrag Pavlovic on 2/9/13.
//  Copyright (c) 2013 Rezedo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RZDCard.h"
#import "RZDDeck.h"

@interface RZDCardMatchingGame : NSObject

// designated initializer
- (instancetype)initWithCardCount:(NSUInteger)cardCount
              usingDeck:(RZDDeck *)deck
               gameMode:(NSUInteger)mode;

@property (nonatomic, readonly) int score;
@property (strong, nonatomic, readonly) NSString *flipResult;
@property (strong, nonatomic, readonly) NSArray *matchedCards; // of Card
@property (nonatomic, readonly) NSInteger flipPoints;
@property (nonatomic, getter = isMarkedForDeletion) BOOL markedForDeletion;

- (NSUInteger)cardCount;
- (void)flipCardAtIndex:(NSUInteger)cardIndex;
- (RZDCard *)cardAtIndex:(NSUInteger)cardIndex;
- (void)removeCardsAtIndexes:(NSIndexSet *)indexSet;
- (NSIndexSet *)indexesOfCards:(NSArray *)selectedCards;    // of Card
- (BOOL)addCards:(NSUInteger)numberOfCardsToAdd;

@end
