//
//  CardCollectionHeaderView.m
//  GraphicalCardGame
//
//  Created by Predrag Pavlovic on 4/26/13.
//  Copyright (c) 2013 Rezedo. All rights reserved.
//  not used because of Apple's bug

#import "CardCollectionHeaderView.h"

@implementation CardCollectionHeaderView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
