//
//  CardCollectionHeaderView.h
//  GraphicalCardGame
//
//  Created by Predrag Pavlovic on 4/26/13.
//  Copyright (c) 2013 Rezedo. All rights reserved.
//  not used because of Apple's bug

#import <UIKit/UIKit.h>

@interface CardCollectionHeaderView : UICollectionReusableView

@property (weak, nonatomic) IBOutlet UIImageView *headerImage;
@property (weak, nonatomic) IBOutlet UILabel *headerLabel;

@end
