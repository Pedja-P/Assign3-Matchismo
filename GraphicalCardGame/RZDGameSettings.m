//
//  RZDGameSettings.m
//  GraphicalCardGame
//
//  Created by Predrag Pavlovic on 3/17/13.
//  Copyright (c) 2013 Rezedo. All rights reserved.
//

#import "RZDGameSettings.h"

@interface RZDGameSettings ()

@end

@implementation RZDGameSettings

#define ALL_SETTINGS_KEY @"GameSettings_All"
#define SHOW_FLIP_COUNT_KEY @"FlipCount"
#define STARTING_CARD_COUNT_KEY @"CardCount"

// singleton object
+ (instancetype)sharedGameSettings
{
    static RZDGameSettings *sharedInstance = nil; // = nil is not needed
    if (!sharedInstance) {
        id plist = [[NSUserDefaults standardUserDefaults] dictionaryForKey:ALL_SETTINGS_KEY];
        sharedInstance = [[self alloc] initFromPropertyList:plist];
    }
    return sharedInstance;
}

// convenience initializer
- (id)init
{
    id plist = [[NSUserDefaults standardUserDefaults] dictionaryForKey:ALL_SETTINGS_KEY];
    self = [self initFromPropertyList:plist];
    return self;
}

// designated initializer
#define STARTING_CARD_COUNT 22
- (instancetype)initFromPropertyList:(id)plist
{
    self = [super init];
    if (self) {
        if ([plist isKindOfClass:[NSDictionary class]]) {
            NSDictionary *settingsDictionary = (NSDictionary *)plist;
            _flipCountEnabled = [settingsDictionary[SHOW_FLIP_COUNT_KEY] boolValue];
            _startingCardCount = [settingsDictionary[STARTING_CARD_COUNT_KEY] unsignedIntegerValue];
        } else {
            _flipCountEnabled = YES;
            _startingCardCount = STARTING_CARD_COUNT;
        }
    }
    return self;
}

// synchronizes current game settings with standardUserDefaults
- (void)synchronize
{
    NSDictionary *settingsDictionary = [[NSDictionary alloc] initWithDictionary:[self asPropertyList]];
    [[NSUserDefaults standardUserDefaults] setObject:settingsDictionary forKey:ALL_SETTINGS_KEY];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

// returns GameSettings as plist
- (id)asPropertyList  // it's id because later we may change the structure
{
    return @{SHOW_FLIP_COUNT_KEY : @(self.isFlipCountEnabled),
             STARTING_CARD_COUNT_KEY : @(self.startingCardCount)};
}

@end
