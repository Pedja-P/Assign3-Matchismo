//
//  RZDGameResult.m
//  GraphicalCardGame
//
//  Created by Predrag Pavlovic on 3/9/13.
//  Copyright (c) 2013 Rezedo. All rights reserved.
//

#import "RZDGameResult.h"

@interface RZDGameResult ()

@property (strong, nonatomic, readwrite) NSDate *start;
@property (strong, nonatomic, readwrite) NSDate *end;
// do not need readwrite duration, because it will always be calculated
@property (strong, nonatomic, readwrite) NSString *name;

@end

@implementation RZDGameResult

#define ALL_RESULTS_KEY @"GameResult_All"
#define START_KEY @"StartDate"
#define END_KEY @"EndDate"
#define SCORE_KEY @"Score"
#define GAME_KEY @"GameName"
#define MATCH_GAME @"Match"
#define SET_GAME @"Set";

// returns an array of all game results
+ (NSArray *)allGameResults
{
    NSMutableArray *allGameResults = [[NSMutableArray alloc] init];
    for (id plist in [[[NSUserDefaults standardUserDefaults] dictionaryForKey:ALL_RESULTS_KEY]
                      allValues]) {
        RZDGameResult *result = [[self alloc] initFromPropertyList:plist];
        [allGameResults addObject:result];
    }
    return allGameResults;
}

// returns the name of Match game as string
+ (NSString *)gameMatchName
{
    // static can be initialized with string literal
    static NSString *gameMatchName = MATCH_GAME;
    return gameMatchName;
}

// returns the name of Set game as string
+ (NSString *)gameSetName
{
    static NSString *gameSetName = SET_GAME;
    return gameSetName;
}

// returns valid name games as array
+ (NSArray *)validGameNames
{
    static NSArray *validGameNames = nil;
    if (!validGameNames) {
        validGameNames = @[[self gameMatchName], [self gameSetName]];
    }
    
    return validGameNames;
}

//convenience initializer
- (instancetype)initFromPropertyList:(id)plist
{
    self = [self init];
    if (self) {
        if ([plist isKindOfClass:[NSDictionary class]]) {
            NSDictionary *resultDictionary = (NSDictionary *)plist;
            _start = resultDictionary[START_KEY];
            _end = resultDictionary[END_KEY];
            _score = [resultDictionary[SCORE_KEY] intValue];
            _name = resultDictionary[GAME_KEY];
            if (!_start || !_end) self = nil;
        }
    }
    return self;
}

//convenience initializer
- (instancetype)initWithGameName:(NSString *)name
{
    self = [self init];
    if (self) {
        if ([[[self class] validGameNames] containsObject:name]) _name = name;
    }
    return self;
}

// designated initializer
- (id)init
{
    self = [super init];
    if (self) {
        // in init methods we shouldn't call setters and getters
        _start = [NSDate date]; // current date/time
        _end = _start;
        _name = @"";
    }
    return self;
}

// duration getter, calculated
- (NSTimeInterval)duration
{
    return [self.end timeIntervalSinceDate:self.start];
}

// score setter
- (void)setScore:(int)score
{
    _score = score;
     
    // we are using setter because we are in setter of another property
    self.end = [NSDate date];
    [self synchronize];
}

// syncronizes data between game and standardUserDefaults
- (void)synchronize
{
    NSMutableDictionary *mutableGameResultsFromUserDefaults = [[[NSUserDefaults standardUserDefaults]
                                                                dictionaryForKey:ALL_RESULTS_KEY]
                                                               mutableCopy];
    if (!mutableGameResultsFromUserDefaults) {
        mutableGameResultsFromUserDefaults = [[NSMutableDictionary alloc] init];
    }
    
    // NSDate can't be a key in dictionary, but NSString can
    // start date as NSString is a good key because we can't start two games
    // at the same time
    mutableGameResultsFromUserDefaults[[self.start description]] = [self asPropertyList];
    [[NSUserDefaults standardUserDefaults] setObject:mutableGameResultsFromUserDefaults forKey:ALL_RESULTS_KEY];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

// returns GameResult as property list
- (id)asPropertyList  // it's id because later we may change the structure of plist
{
    return @{START_KEY : self.start, END_KEY : self.end, SCORE_KEY : @(self.score), GAME_KEY : self.name};
}

#pragma mark - Sorting

// compares two results by scores
- (NSComparisonResult)compareScoreToGameResult:(RZDGameResult *)otherResult
{
    if (self.score > otherResult.score) {
        return NSOrderedAscending;
    } else if (self.score < otherResult.score) {
        return NSOrderedDescending;
    } else {
        return NSOrderedSame;
    }
}

// compares two results by end date
- (NSComparisonResult)compareEndDateToGameResult:(RZDGameResult *)otherResult
{
    return [otherResult.end compare:self.end]; // compare returns NSComparisonResult a ne BOOL
}

// compares two results by duration
- (NSComparisonResult)compareDurationToGameResult:(RZDGameResult *)otherResult
{
    if (self.duration > otherResult.duration) {
        return NSOrderedDescending;
    } else if (self.duration < otherResult.duration) {
        return NSOrderedAscending;
    } else {
        return NSOrderedSame;
    }
}

@end
