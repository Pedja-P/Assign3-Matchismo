//
//  RZDGameSettings.h
//  GraphicalCardGame
//
//  Created by Predrag Pavlovic on 3/17/13.
//  Copyright (c) 2013 Rezedo. All rights reserved.
//  Helper class for GameSettingsViewController.
//

#import <Foundation/Foundation.h>

@interface RZDGameSettings : NSObject

+ (instancetype)sharedGameSettings; // name should be sharedMyClass or sharedInstance

- (instancetype)initFromPropertyList:(id)plist;   // designated initializer

@property (nonatomic, getter = isFlipCountEnabled) BOOL flipCountEnabled;
@property (nonatomic) NSUInteger startingCardCount;

- (void)synchronize;

@end
