//
//  RZDAppDelegate.h
//  GraphicalCardGame
//
//  Created by Predrag Pavlovic on 3/30/13.
//  Copyright (c) 2013 Rezedo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RZDGraphicalCardGameAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
