//
//  RZDCardGameViewController.h
//  GraphicalCardGame
//
//  Created by Predrag Pavlovic on 2/1/13.
//  Copyright (c) 2013 Rezedo. All rights reserved.
//

#import <UIKit/UIKit.h> // all UI classes
#import "RZDDeck.h"

@interface RZDCardGameViewController : UIViewController

// all of the following methods must be overridden by concrete subclasses
@property (readonly, nonatomic) NSUInteger startingCardCount;   // abstract
@property (nonatomic, getter = isDeletionOfCardsEnabled) BOOL deletionOfCardsEnabled;

- (NSString *)gameName; // abstract
- (RZDDeck *)createDeck;   // abstract
- (NSUInteger)gameMode; // abstract
- (void)updateCell:(UICollectionViewCell *)cell usingCard:(RZDCard *)card animated:(BOOL)animated;  // abstract
- (NSAttributedString *)convertResultStringToAttributed:(NSString *)resultString
                                           matchedCards:(NSArray *)matchedCards;    // abstract
- (void)addCard:(RZDCard *)card resultView:(UIView *)view inRect:(CGRect)rect;  // abstract
- (void)updateFoundMatchesViewCell:(UICollectionViewCell *)cell matchedCards:(NSArray *)matchedCards;    // abstract

@end
